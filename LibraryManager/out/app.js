"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var enums_1 = require("./enums");
var classes_1 = require("./classes");
function GetAllBooks() {
    var books = [
        { id: 1, title: 'Ulysses', author: 'James Joyce', available: true, category: enums_1.Category.Fiction },
        { id: 2, title: 'Ulysses', author: 'James Joyce', available: true, category: enums_1.Category.Fiction },
        { id: 3, title: 'Ulysses', author: 'James Joyce', available: true, category: enums_1.Category.Poetry },
        { id: 4, title: 'Ulysses', author: 'James Joyce', available: true, category: enums_1.Category.Fiction }
    ];
    return books;
}
function LogFirstAvailable(books) {
    var firstAvailable = '';
    var numberOfBooks = books.length;
    for (var _i = 0, books_1 = books; _i < books_1.length; _i++) {
        var currentBook = books_1[_i];
        if (currentBook.available) {
            firstAvailable = currentBook.title;
            break;
        }
    }
    console.log('Total Books: ' + numberOfBooks);
    console.log('First Available: ' + firstAvailable);
}
function GetBookTitlesByCategory(categoryFilter) {
    var filteredTitles = [];
    for (var _i = 0, allBooks_1 = allBooks; _i < allBooks_1.length; _i++) {
        var currentBook = allBooks_1[_i];
        if (currentBook.category === categoryFilter) {
            filteredTitles.push(currentBook.title);
        }
    }
    return filteredTitles;
}
function GetBooksByID(id) {
    var allBooks = GetAllBooks();
    return allBooks.filter(function (book) { return book.id === id; })[0];
}
function PrintBook(book) {
    console.log(book.title + ' by ' + book.author);
}
//****************************************************
var myBook = {
    id: 5,
    title: '',
    author: '',
    available: true,
    category: enums_1.Category.Fiction
};
// PrintBook(myBook);
// myBook.markDamaged('torn pages');
var logDamage;
logDamage = function (damage) { return console.log('Damage reported ' + damage); };
logDamage('coffee stains');
var favouriteLibrarian = new classes_1.UniversityLibrarian();
favouriteLibrarian.name = 'Sharon';
favouriteLibrarian.assistCustomer('Lynda');
var allBooks = GetAllBooks();
LogFirstAvailable(allBooks);
var fictionBooks = GetBookTitlesByCategory(enums_1.Category.Fiction);
fictionBooks.forEach(function (val, idx, arr) { return console.log(++idx + ' - ' + val); });
var firstBook = GetBooksByID(1);
// let ref: ReferenceItem = new ReferenceItem('Facts and Figures', 2015);
// ref.printItem();
// ref.publisher = 'Random Data Publishing'
// console.log(ref.publisher);
var refBook = new classes_1.Encyclopedia('Facts and Figures', 2015, 10);
refBook.printItem();
refBook.printCitation();
var Newspaper = /** @class */ (function (_super) {
    __extends(class_1, _super);
    function class_1() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    class_1.prototype.printCitation = function () {
        console.log("Newspaper: " + this.title);
    };
    return class_1;
}(classes_1.ReferenceItem));
var myPaper = new Newspaper('The Gazette', 2019);
myPaper.printCitation();
var Novel = /** @class */ (function (_super) {
    __extends(Novel, _super);
    function Novel() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return Novel;
}(/** @class */ (function () {
    function class_2() {
    }
    return class_2;
}())));
var favouriteNovel = new Novel();
//# sourceMappingURL=app.js.map