import { Book, DamageLogger, Person, Author, Librarian } from './interfaces';

class UniversityLibrarian implements Librarian {
  name: string;
  email: string;
  department: string;

  assistCustomer(custName: string) {
    console.log(this.name + ' is assisting ' + custName)
  }
}

abstract class ReferenceItem {

  protected _publisher: string;
  static department: string = 'Research';

  constructor(public title: string, protected year: number) {
    console.log('Creating a new ReferenceItem...')
  }

  get publisher(): string {
    return this._publisher.toUpperCase();
  }

  set publisher(newPublisher: string) {
    this._publisher = newPublisher;
  }

  printItem(): void {
    // ECMAScript 2015 feature - template strings - these are supported in Typescript
    console.log(`${this.title} was published in ${this.year}`)
    console.log(`Department: ${ReferenceItem.department}`)
  }

  abstract printCitation(): void;
}

class Encyclopedia extends ReferenceItem {

  constructor(newTitle: string, newYear: number, public edition: number) {
    super(newTitle, newYear);
  }

  // override method on parent class
  printItem(): void {
    // call parent method
    super.printItem();
    console.log(`Edition: ${this.edition} (${this.year})`);
  }
  printCitation(): void {
    console.log(`${this.title} - ${this.year}`);

  }
}
export { UniversityLibrarian, ReferenceItem, Encyclopedia };