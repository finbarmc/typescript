import { Category } from './enums';
import { Book, DamageLogger, Person, Author, Librarian } from './interfaces';
import { UniversityLibrarian, ReferenceItem, Encyclopedia  } from './classes';

function GetAllBooks(): Book[] {

  let books = [
    { id: 1, title: 'Ulysses', author: 'James Joyce', available: true, category: Category.Fiction},
    { id: 2, title: 'Ulysses', author: 'James Joyce', available: true, category: Category.Fiction},
    { id: 3, title: 'Ulysses', author: 'James Joyce', available: true, category: Category.Poetry},
    { id: 4, title: 'Ulysses', author: 'James Joyce', available: true, category: Category.Fiction}
  ];
  return books;
}

function LogFirstAvailable(books): void{
  let firstAvailable = '';
  let numberOfBooks = books.length;
  for(let currentBook of books) {
    if (currentBook.available) {
      firstAvailable = currentBook.title;
      break;
    }
  }
  console.log('Total Books: ' + numberOfBooks);
  console.log('First Available: ' + firstAvailable);
}

function GetBookTitlesByCategory(categoryFilter: Category): Array<object> {

  let filteredTitles = [];

  for (let currentBook of allBooks) {
    if (currentBook.category === categoryFilter) {
      filteredTitles.push(currentBook.title);
    }
  }
  return filteredTitles;
}

function GetBooksByID(id: number) {
  const allBooks = GetAllBooks();
  return allBooks.filter(book => book.id === id)[0];
}

function PrintBook(book: Book): void {
  console.log(book.title + ' by ' + book.author);
}

//****************************************************

let myBook: Book = {
  id: 5,
  title: '',
  author: '',
  available: true,
  category: Category.Fiction
}

// PrintBook(myBook);
// myBook.markDamaged('torn pages');

let logDamage: DamageLogger;
logDamage = (damage: string) => console.log('Damage reported ' + damage);
logDamage('coffee stains');

let favouriteLibrarian: Librarian = new UniversityLibrarian();
favouriteLibrarian.name = 'Sharon';
favouriteLibrarian.assistCustomer('Lynda');

const allBooks = GetAllBooks();
LogFirstAvailable(allBooks);

const fictionBooks = GetBookTitlesByCategory(Category.Fiction);
fictionBooks.forEach((val, idx, arr) => console.log(++idx + ' - ' + val) );

let firstBook = GetBooksByID(1);

// let ref: ReferenceItem = new ReferenceItem('Facts and Figures', 2015);
// ref.printItem();
// ref.publisher = 'Random Data Publishing'
// console.log(ref.publisher);

let refBook = new Encyclopedia('Facts and Figures', 2015, 10);
refBook.printItem();
refBook.printCitation();


let Newspaper = class extends ReferenceItem {
  printCitation(): void {
    console.log(`Newspaper: ${this.title}`);
  }
}

let myPaper = new Newspaper('The Gazette', 2019);
myPaper.printCitation();

class Novel extends class { title: string } {
  mainCharacter: string;
}

let favouriteNovel = new Novel();