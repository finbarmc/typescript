# Typescript Notes
These notes are based on the Pluralsight course [TypeScript In Depth](https://app.pluralsight.com/library/courses/typescript-in-depth/table-of-contents).

## Variables and Constants
Differences between
- var
- let
- const

### Differences
- var is the common way that's used to create variables in Javascript. Variables created with var are globally available in the function in which they are declared. On the other hand, variables declared with Let and const are only available in the block in which they are declared
- Variables created with var are hoisted, so that they are actually declared at the top of a function, regardless of where they are specified. This isn't the case with let or const
- Variables created with var can be created multiple times in the same scope. This isn't the case with let or const

Variables are declared using the syntax
```typescript
let myVar: string = 'this is a string';
```

## Data Types
- Boolean
- Number
- String
- Array
- Enum
- Any (Means you can assign negate use of the type system and assign anything to this kind of variable)

Typescript supports type inference based on what you initialise a variable with. This is also the case with functions that don't explicitly declare their own return type. Type annotations do make code more readable so should be used regardless.



## Basic Data Structures
- enums
- array
- tuples

### Enums
Enums can be declared using the syntax
```typescript
enum Category { Biography, Poetry, Fiction };
```

You can specify bespoke values for an enum, either for the first item or all items
```typescript
enum Category { Biography = 1, Poetry, Fiction };
enum Category { Biography = 3, Poetry = 5, Fiction = 8 };
```

Logging out an enum gives you the associated int value.
```typescript
let favouriteCategory: Category = Category.Biography
console.log(favouriteCategory)
```

You can access the string value using this syntax
```typescript
let categoryString = Category[favouriteCategory]; // Biography
```

### Arrays
Arrays in Typescript can be created in a number of ways:
```typescript

// declared using string type
let strArray1: string[] = ['here','are','string'];

// declared using Array keyword and string generic
let strArray2: Array<string> = ['more','strings','here'];

// declared using any type so it can hold different typed data
let strArray3: any[] = [42,'strings'];

```

### Tuples
You can think of tuples as an array where the first few elements are specified. Types after that don't have to be the same.
```typescript

let myTuple: [number, string] = [25, 'are'];

let firstElemenet = myTuple[0];

let secondElemenet = myTuple[1];

// other elements can have numbers OR strings e.g.
let myTuple[2] = 100;
let myTuple[2] = 'this works!';
```

## Functions

### Functions Typescript vs Javascript
- Typescript functions allow types
- Typescript allows use of arrow functions - these are only available in ES2015 Javascript
- Required and optional parameters are supported in Typescript. In Javascript all parameters are optional
- Default parameters are available in Typescript - these are only available in ES2015 Javascript
- Rest parameters are available in Typescript - these are only available in ES2015 Javascript

### Parameters and Return Types
Below is an example of a function declaration in Typescript using assigned types

```typescript
function CreateCustomer(name: string, id: number): string {}

```

### Arrow Functions
Arrow functions provide a concise syntax for anonymous functions. 'Given these parameters, do this'.

```typescript
// no parameters
let arr = allBooks.filter(() => console.log('some stuff'));

// one parameter
let arr = allBooks.filter(book => book.author === 'JR Hartley');

// multiple parameters
let arr = allBooks.filter((book, title) => console.log(book + ' ' + title));

// multiple parameters, multiple lines
let arr = allBooks.filter((book, title) => {
  console.log('stuff');
  console.log(book + ' ' + title);
});
```

One gotcha when using `this` with Javascript is that by the time a callback function has been invoked, this has changed. In Typescript, this is captured at function creation, not at invocation, which means you don't have to use `var self = this` in your code.

### Function Types
Function Types are a combination of parameter types and return types. Variables may be declared with function types.

```typescript

// function declaration
function PublicationMessage(year: number): string {
  return 'Date Published: ' + year;
}

// variable containing a function - declared specifying the shape of the function that can be assigned
let publishFunc: (someYear: number) => string;

publishFunc = PublicationMessage;

// function invocation
let message: string = publishFunc(2016);

```

### Parameters
Optional parameters are denoted by use of a question mark. The must appear after required parameters in the function declaration e.g.

```typescript
// function with 1 optional parameter
function createCustomer(name:string, age?: number) { }

// function with 1 implicit optional parameter - default value
function GetBookByTitle(title:string = 'The C Programming Language') { }

// function with 1 implicit optional parameter - default value from function
function GetBookByTitle(title:string = GetDefaultTitle()) { }

```

Rest Parameters are used to provide an indeterminate amount of parameters to a function. These are denoted with the elipsis operator.

```typescript
// function with 1 parameter and ids rest parameter
function GetBooksReadForCustomer(name:string, ...bookIDs: number[]) { }

let books = GetBooksReadForCustomer('Daniel', 2, 5);

let books = GetBooksReadForCustomer('Daniel', 2, 5, 12, 42);

```

### Overloading Functions
Function overloading is a common langaguge feature that allows multiple functions with the same name to exist. This exists in Typescript, but as it's transpiled into Javascript where this isn't supported there are some limitations. Whilst different function declarations can be made, only one implementation function can be provided.

The implementing function is coded in such a way as to support the entire interface.
```typescript
//
function GetTitles(author: string): string[];

function GetTitles(available: boolean): string[];

// this is the implementation function
function GetTitles(bookProperty: any): string[] {
  if (typeof bookProperty == 'string') {
    // get by author and add to foundTitles
  }
  else if (typeof bookProperty == 'boolean') {
    // get by availability and add to foundTitles
  }
  return foundTitles;
}
```

## Interfaces
Interfaces are the contract that define how a type can/can't be used. They aren't compiled to anything in typescript, but are used by the compiler for type checking.
As in other languages, they are just a collection of property and method declarations, providing no implemetation details.

Interfaces provide a form of duck typing - as long as an object provides the shape of a type, it can be used in place of that interface.

### Usage of Interfaces

### Duck Typing
When I see a bird that walks like a duck and swims like a duck and quacks like a duck, I call that bird a duck.
The duck test means that a object that matches the shape of a type can be used in place of that type, even though they may not have been explicitly declared of that type.

Even if you declare an object without the specific type, if it meets the shape of the interface, it can be used in place.

### Defining Interfaces
You can declare an interface as below:

```typescript
//
interface Book {
  id: number;
  title: string;
  author: string;
  // optional parameter
  pages?: number;
  // method returning void
  markDamaged: (reason: string) => void;
}

```

Interfaces can be consumed like:
```typescript
//
let myBook: Book = {
  id: 4;
  title: 'Game of Thrones';
  author: 'Big Dirty George';
  // optional parameter
  pages?: 250;
  // method returning void
  markDamaged: (reason: string) => void {
    console.log('book damaged');
  }
}

```
### Interfaces for Function Types
You can use interfaces for methods as well as classes, which is useful for ensuring that a specific variable can only be assigned to a specific shape of function.

```typescript
function CreateCustomerId(name: string, id: number): string {
  return name + id;
}

// this is a function type interface
// note that the return type is specified using a colon here rather than an arrow =>
interface StringGenerator {
  (chars: string, nums: number) : string;
}

// assign the variable with a function type interface
let IdGenerator: StringGenerator;

// can assign this to our variable
IdGenerator = CreateCustomerID;

// CAN'T assign this to our variable
IdGenerator = function(name: number, id: string): void {
};

```

### Extending Interfaces
```typescript
interface LibraryResource {
  catalogNumber: number;
}

interface Book {
  title: string;
}

// interface that extends those above
interface Encyclopedia extends LibraryResource, Book {


}
```

### Interfaces For Class Types
Class types can work in conjunction with interfaces to create

```typescript

interface Librarian {
  doWork: () => void;
}

class ElementarySchoolLibrarian implements Librarian {
  doWork() {
    console.log('Teaching kids');
  }
}

let kidsLibrarian: Librarian = new ElementarySchoolLibrarian();
kidsLibrarian.doWork();

```


## Classes
As per any OO languages, they serve as a template for creating objects. They provide state storage and behaviour and encapsulate reusable functionality for a given entity. They help us think about our programs in simpler terms in the context of our domain.

### Class Constructors
Constructors are classes that perform initialization for new instances of a class. Typescript supports one constructor per class. You can use optional parameters to offer different shaped interfaces for constructors.

```typescript
class Item {

  constructor(title: string, publisher?: string) {
    // initialization here
  }
}
let myItem = new Item('my title');

```

### Class Members
Properties and methods are supported in typescript. Some examples are below.

```typescript
class Item {
  // this is a member
  numberOfPages: number;

  // editor property
  get editor(): string {
    // custom getter logic goes here
  }
  set editor(newEditor: string): string {
    // custom setter logic goes here
  }

  // this is a method
  printChapterTitle(chapterNum: number): void {
    // print title here
  }

  constructor(title: string, publisher?: string) {
    // initialization here
  }
}
let myItem = new Item('my title');

```

### Access Modifiers

You can declare members explicitly using the public keyword, but by default all members are declared public, which is a departure from Java/C#.
Private (only available in that class) and Protected (only available in child classes) access modifiers are also supported.

Constructor Parameters offer a shorthand for declaring a property inside the constructor. The public keyword tells the compiler to create a property with the same name as the property and that it should be assigned in the constuctor.
You can use Constructor Parameters to create private fields as well

```typescript
class Author {
  constructor(public name: string) {
    // stuff and things
  }
}
```

### Static Properties
Static Properties are supported using the `static` keyword e.g.

```typescript
class Author {
  constructor(public name: string) {
    // stuff and things
  }
  static description: string = 'A source of knowledge';
}
let desc = Author.description;
```

### Inheritance
As per standard OO, this is the mechanism for a parent class to share functionality with it's derived classes.
```typescript
class ReferenceItem {
  title: string;
  printItem(): void {}
}

// derived class
class Journal extends ReferenceItem {

  // call parent constructor. If a child class has a constructor it must call super, even if the parent doesn't have one
  constructor() {
    super();
  }
}

```

### Abstract Classes
These are special classes that cannot be instantiated, but can contain implementation details. These can contain methods that aren't implemented, but must be implemented in derived classes.
```typescript
abstract class ReferenceItem {
  title: string;
  printItem(): void {}
}

// derived class
class Journal extends ReferenceItem {

  // call parent constructor. If a child class has a constructor it must call super, even if the parent doesn't have one
  constructor() {
    super();
  }
}
```

### Class Expressions
Class expressions are a ES2015 feature that are also supported in Typescript. They allow you to define and extend a class inline.

```typescript

// newspaper extends ReferenceItem class using class expression
let Newspaper = class extends ReferenceItem {
  printCitation(): void {
    console.log(`Newspaper: ${this.title}`);
  }
}

let myPaper = new Newspaper('The Gazette', 2019);
myPaper.printCitation();

// novel extends an anonymous class using a class expression
class Novel extends class { title: string } {
  mainCharacter: string;
}

let favouriteNovel = new Novel();

```

## Modules and Namespaces
These features help us to organise our code so that is reuseable.

### History of Modules in Typescript
Version 1.5 of Typescript introduced major changes to modules. Prior to 1.5 it had both internal and external modules. After 1.5 internal modules became 'namespaces' and external modules became 'modules'. Support for ES2015 modules has been added to Typescript.

### Modules vs Namespaces
Modules and Namespaces are tools for organising code.

Modules have support in Node and in the browser through use if a module loader. Typescript modules support the ES2015 module syntax which makes them future proof.

Namespaces don't require a special loader. Their purpose is to  prevent pollution of the global scope. Namespaces are a good option for smaller sized client applications.

### Creating and using Namespaces
Namespaces are declared using the namespace keyword. They can also be nested - see below.

```typescript
namespace Membership {
  export function AddMember(name: string) {
    // function code here
  }
  // nested namespace
  export namespace Cards {
    export function IssueCard(memberNumber: number) {
      // function code here
    }
  }

}

// call function in top level namespace
Membership.AddMember('Dave');

// call function in nested namespace
Membership.Cards.IssueCard(1234);
```

### Triple Slash References
These are used to reference a namespace declared in another file in your calling code. If the above code was declared in a file called membership.ts it would be included using the syntax below.

```typescript
/// <reference path="membership.ts" />

// this file can make a call to functions in the top level namespace
Membership.AddMember('Dave');

// and also to nested namespaces
Membership.Cards.IssueCard(1234);
}

```

The triple slash reference tells the Typescript compiler to compile all required referenced files for you. The -outFile compiler option can be used to generate a single JS output file.
```typescript
// creates a single file from app.ts and it's references files
tsc --target ES5 app.ts -- outFile out.js
```

### Creating and using Modules
Modules make code more maintainable and reusable. THey are native to Node and ES2015 and Typescript compiles to a format that are supported by both of those. They are organized simply using the file system.

There are multiple different module formats - and they are quite complex. Typescript can compile to all supported formats - see below
- Node uses the common.js module format
- Asynchronous Module Definition (AMD) is a popular format for browser based apps. This is sometimes used with the require.js module loader
- Universal Module Definition (UMD) is a format that combines the two above and can be loaded by both Node and AMD
- System.js is a popular module format that supports the common.js and AMD formats and also defines it's own custom format
- Outputting to ES2015 module format is also supported. This is the syntax that is supported natively in Typescript

### Exporting from a Module
Items from a module are only available to be used in other modules if they are exported.
This can be achieved in a number of ways:
- Export declarations are the simplest way to export from a module e.g.

```typescript
export interface Periodical {
  issueNumber: number;
}

export class Magazine implements Periodical {
  issueNumber: number;
}

export function GetMagByIssueNo(issue: number): Magazine {

}

```

- An export statement at the end of a file
```typescript
// export items - can export using an alias
export { Periodical, Magazine, GetMagByIssueNo as GetMag}

```

### Importing from a Module
You can import code into a module using the `import` keyword e.g.

```typescript
// import items - this can also make use of aliases
import { Periodical, Magazine, GetMagByIssueNo as GetMag} from './periodicals';

// then use as below
let newsMag: Magazine = GetMagazine('Football Magazine');

// import all using relative path to the file
import * as mag from './periodicals';

```

### Default Exports
There are most useful when you only want to export a single item from a module. This is exported using the `default` keyword. It can be used with the name assigned in the import statement.

```typescript
// movie.ts
export default class {
  title: string;
  director: string;
}

// kids.ts
import AnimatedMovie from './movie';
let myVar = new AnimatedMovie();
```

## Generics
### What are Generics
These are code that is designed to work with multiple types instead of a single type. The accept type parameters which specify the type a generic will operate over. A class can be instantiated multiple times with different type parameters. They are passed with angle branckets separate from function parameters.

### Example Generics
Below is an example of creation of an array using Generics.
```typescript
// array created using array operator
let poetryBools: Book[];

// array created using array generic
let fictionBooks: Array<Book>;
```

### Generic Functions
Below is an example of a generic function
```typescript
function LogAndReturn<T>(thing: T): T {
  console.log(thing);
  return thing;
}

let someString: string = LogAndReturn<string>('log this');
let someString: Magazine = LogAndReturn<Magazine>(my Mag);

```

### Generic Classes and Interfaces
This is much the same as in C#, because TypeScript is a Microsoft language.

```typescript
interface Inventory<T> {
  getNewestItem: () => T;
  addItem: (newItem: T) => void;
  getAllItems: () => Array<T>;
}

let bookInventory: Inventory<Book>;

class Catalog<T> implements Inventory<T> {
  private catalogItems = new Array<T>();
  // implement members ...
}
let bookCatalog: new Catalog<Book>;

```
### Generic Constraints
This allows us to apply constraints so that we have control over the types that may be passed as a generic type parameter. In the exmaple below, only types that are derived from CatalogItem can be passed as a generic type parameter.
```typescript
class Catalog<T extends CatalogItem> implements Inventory<T> {
}
```

## Compiler Options

### Common Compiler Options
- To specify the module format use the flag --module or --m. Supported formats are common or AMD etc.
- Specify how the compiler should resolve modules using the flag --moduleResolution e.g. Node or Classic
- Specify the version of js that should be output by the compiler using the --target flag e.g. ES15
- Specify the --watch or --w flag so that when any files change, the files are automatically converted into js
- Specify a directory where output files should be written to using the --outDir flag
- You can force a compiler error where type inference would implicitly infer an Any type using the --noImplicitAny flag. All Any types have to be declared explictly with this.

See the [TypeScript GitHub](https://github.com/Microsoft/TypeScript-Handbook/blob/master/pages/Compiler%20Options.md) for more detail on compiler options.

### Use of tsconfig.json
This is a simple json file that marks the root of a typescript project and specify the settings to applied to the project. The convention is for the compiler to look for a file of this name in the file system. This is where we can configure the compiler and control which files should be built.

The following options are supported in this file:
- compilerOptions - options to configure the TypeScript compiler
- files - files to include in the built
- exclude - files/directories to exclude from the build

## Type Definitions
Type definitions help you integrate external js code into your project. These are files that contain type information for a library you want to consume. They don't contain any explicit implementation details, just the interfaces required for the compiler to type check your code. The are primarily used as a TypeScript wrapper for pure Javascript libraries.

The main benefit is as a design time tool for type checking and in editor support. They follow the convention of .d.ts for their file extensions.

It's quite common the the Type Definition files are developed by different people than those who maintain the actual library - usually they are created by consumers of a library that want to use it in TypeScript. This can lead to mismatches. The largest repo of these are is DefinitelyTyped on GitHub. Typings Type Definitions Manager is another. This is also supported from npm - so that they can come down directly with a library, but not many authors provide this support out of the box.

### Ambient Modules
These are modules that are created with the declare keyword and don't define an implementation.

```typescript
// cardCatalog.d.ts
declare module "CardCatalog" {
  export function printCard(callNumber: string): void;
}

// app.ts

/// <reference path="cardCatalog.d.ts" />
import * as catalog from "CardCatalog";

```

### Managing Type Definitions with DefinitelyTyped
This is a huge repo of type definitions in GitHub. This can be included directly from GitHub, via Nuget, via tsd (a manager designed to work with DefinitelyTyped) or via typings.

### Managing Type Definitions with tsd
This has been deprecated, nothing to see here.

### Managing Type Definitions with typings
typings allows you to find and download type definitions from multiple sources. It manages references to installed definitions and stores triple slash references in a single file.